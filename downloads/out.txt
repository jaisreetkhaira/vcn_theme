Vancouver Community Network
Home
Services
Donate
Who we are
Help
Get Involved
Programs
Community Resources
Webteam
Contact Us
Services For Everyone
Resources for Technical Volunteers in Non-Profit Organizations
Troubleshooting
Test Sign-up
Windows 95/98/ME
Receiving duplicate/same messages every time
How to check your inbox size
Test Registration
Services For Non-Profit Organizations
Services For Groups
Managing an Account
Dial-up
email
Sign Up
Web Pages
Mailing List
Security Software
Other Services
Memberships
Volunteer
Volunteer and Job Opportunities at VCN
Youth Initiatives Program
Past Initiatives
Community Access Program (CAP)
Freenet
Policy
Admin Log-in
Sitemap
VCN Knowledgebase
Articles
VCN Account
VCN Dial Up
Email
Mailing List
Web Hosting
Donations
Other Services
Map of Interest
Peter Royce
User Agreement
Techteam
Board of Directors
Minutes
VCN Newsletter
vancouver-community-network-constitution-bylaws
Funders and In-kind Donors
Charitable Status and the VCN
Volunteer Guide
VCN charitable status
Important Excerpts from the Courts decision
Full text of the Courts decision. (fairly long) 
July 10 1996 media release from the CommunityNet.
1993 Letter from Revenue Canada to the National Capital FreeNet, Ottawa, refusing charitable tax status.
what&#039;s involved in being a VCN board member
memorandum of understanding
Common Dial Up Access Issues
Common Issues with VCN Account
Common Issues with VCN Email
Common Mailing List Issues 
Common Web Hosting Issues
Common Issues with Donations
Other common issues
Standard Procedure
VCN Account
Email
VCN Dial up Access
Mailing List
Web Hosting
Home-DialUp
Sign Up for Individuals
Sign Up for Organizations
Community Events
Donation
Other Services
Frequently Asked Questions
VCN Tools
Common Issues with VCN Tools
Street Messaging System
Domain Name Hosting
Free Internet Access via Dial-Up
Free Email Accounts
Free Webmail
Free Web Page Hosting
Free Computer Help - Technical Help Desk
Referrals for Used Computers
Free Group Email Accounts
Free CMS Web Site powered by WordPress
Free Mailing Lists
Low-cost IT support
Domain Name Hosting
Domain Registration
Free Group Email Accounts (POP3)
Free CMS Web Site powered by WordPress
Free Mailing Lists
Low-cost IT support
Email Sign Up for Individuals
Viewing and Changing VCN Account Contact Information
VCN outgoing mail service set up guide
VCN outgoing mail service for Android
VCN outgoing mail service for iPhone
Thunderbird
Apple Mail
Outlook Express
WIndows Live Mail
Gmail
Thunderbird 31 SPAM Filtering
VCN Laptop Lease Request Form
Autoresponder / Email Forwarding
Contact Us
Email Setup for Android
Egor - Mozilla Thunderbird
Outlook 2003
Outlook 2007 on Windows XP, 7 and 8
Outlook 2010
Outlook 2016 on Windows 7 and 8
Using the Quiz and Survey Master plug-in with WordPress
Microsoft Office Outlook 2010
This is a poll test
312 Main St
Email Sign Up for Individuals (Test)
Windows 10
Email Configuration
How to deal with spam (comments)
[new_royalslider id="11"]]]></content:encoded>
<h3 style="text-align: left;">Services</h3>
<h3 style="text-align: left;">Why Donate?</h3>
<h3 style="text-align: left;">Who we are</h3>
<h3 style="text-align: left;">Help</h3>
<h3 style="text-align: left;">Get Involved</h3>
<h3 style="text-align: left;">Programs</h3>
<h3 style="text-align: left;">Community Resources</h3>
<h3 style="text-align: left;"><span style="color: #ff0000;"><span style="color: #000000;">VCN |</span> web<span style="color: #000000;">team</span></span></h3>
<h3 style="text-align: left;">Contact Us</h3>
<h3>Services For Everyone</h3>
<h3 style="text-align: left;">Resources for Technical Volunteers</h3>
The following articles provide troubleshooting tips and solutions to common issues.
<p>Please fill out the information below as accurately as possible. Account requests without a full legal name (on government-issued ID) and address will not be processed.</p>
<ol>
]]></content:encoded>
]]></content:encoded>
<span style="color: #ff0000;">TEST</span>]]></content:encoded>
<span style="color: #ff0000;">text</span>]]></content:encoded>
Please fill out the information below as accurately as possible. Account requests without a full legal name (on government-issued ID) and address will not be processed.
<h3 style="text-align: left;">Services
<h3 style="text-align: left;">Services
<h3 style="text-align: left;">Help
<h3 style="text-align: left;">Help
<h3 style="text-align: left;">Help
<div id="signup">
<h2 style="text-align: left;">Help
<h3 style="text-align: left;">Help
<h3 style="text-align: left;">help
<h3 style="text-align: center;">Help
<h3 style="text-align: left;">Memberships</h3>
<h3 style="text-align: left;">Volunteer Opportunities</h3>
<h3 style="text-align: left;">Volunteer and Job Opportunities at VCN</h3>
<h3>Youth Initiatives Program</h3>
<h3 style="text-align: left;">Community Resources
<h2>Community Access Program (CAP)</h2>
<h3 style="text-align: left;">Community Resources
<h2 style="font-size: 18px; color: #006766; padding: 0px 10px;">PERSONAL INFORMATION PROTECTION POLICY
<h3>Web Site Admin Log In</h3>
<p >
This site contains the standard procedures for completing requests and resolving issues.
]]></content:encoded>
<span style="font-size: 16px;font-weight: bold;line-height: 1.5">Topics</span>
Topics
Topics
Topics
Topics
Topics
Topics
<h3>
<div class="plane-bg" id="peter">
<h3 style="text-align: left;">User Agreement</h3>
<h3 style="text-align: left;"><span style="color: #0000cd;"><span style="color: #000000;">VCN |</span> tech<span style="color: #000000;">team</span></span></h3>
<h3 style="text-align: left;">Board of Directors</h3>
<div class="minutes-page">
VCN publishes a newsletter periodically throughout the year with operational updates, news, frequently asked questions and many other interesting facts.
<h3 style="text-align: left;">VCN – Constitution &amp; ByLaws</h3>
<ul style="color: rgb(56, 56, 56); font-family: Tahoma; font-size: 14px; line-height: 20px;">
<h3 style="text-align: left;">Charitable Status and the VCN</h3>
<a name="top"></a><a href="http://dev.www2.vcn.bc.ca/wp-content/uploads/2014/01/blank.png"><img class="alignnone size-medium wp-image-90" alt="blank" src="http://dev.www2.vcn.bc.ca/wp-content/uploads/2014/01/blank.png" width="10" height="10" /></a>
<h3 style="text-align: left;">VCN Charitable Status application</h3>
<h3 style="text-align: left;">Excerpts from the Reasons for Judgment</h3>
<h3 style="text-align: left;">Full Text of Judgement</h3>
<h3 style="text-align: left;">Media Release 10 Jul 1996</h3>
<h3>STATUS OF REGISTRATION AS A CHARITY</h3>
<h3 style="text-align: left;">Memorandum of Understanding</h3>
The following articles provide troubleshooting tips and solutions to dial up issues.
The following articles provide troubleshooting tips and solutions to VCN Account issues.
The following articles provide troubleshooting tips and solutions to VCN Email issues.
The following articles provide troubleshooting tips and solutions to VCN's Mailing List issues.
The following articles provide troubleshooting tips and solutions to Web Hosting issues.
The following articles provide troubleshooting tips and solutions for issues with donations.
The following articles provide troubleshooting tips and solutions to VCN Account issues.
The following articles discuss the standard procedure for performing routine tasks.
The following articles discuss the standard procedure for performing routine tasks.
The following articles discuss the standard procedure for performing routine tasks.
The following articles discuss the standard procedure for performing routine tasks.
The following articles discuss the standard procedure for performing routine tasks.
The following articles discuss the standard procedure for performing routine tasks.
]]></content:encoded>
Please fill out the information below as accurately as possible. Account requests without a full legal name (on government-issued ID) and address will not be processed.
<h2>Organization Information</h2>
[hide for="!logged"]
The following articles discuss the standard procedure for performing routine tasks.
The following articles discuss the standard procedure for performing routine tasks.
<span style="font-size: 16px; font-weight: bold; line-height: 1.5;">Topics</span>
The following articles discuss the standard procedure for performing routine tasks.
The following articles provide troubleshooting tips and solutions to dial up issues.
<h3>Street Messaging System</h3>
VCN provides groups with a sub-domain (eg. <b><i>vcn.bc.ca/yourgroup</i></b> or <b><i>yourgroup.vcn.bc.ca</i></b> for WebGUI installations) as part of its standard package of services. However, some groups prefer to register their own domain name (eg. <b><i>yourgroup.com</i></b>).
We have three pools of 56K modems to allow users in the toll-free area in Greater Vancouver to get connected to the internet. For help on setting up your computer to dial up to the internet, consult the <a style="text-decoration: none;" href="https://www2.vcn.bc.ca/help/dial-up/">Setting Up Dialup</a> section of the <a style="text-decoration: none;" href="https://www2.vcn.bc.ca/help/managing-an-account/">help pages</a>.
We offer free email accounts to all registered users. To sign up for an email account as an individual or a non-profit group, go to our <a style="text-decoration: none;" href="https://www2.vcn.bc.ca/sign-up/">Sign Up page</a>. You can use POP or IMAP to transfer your mail between your computer and the mail server, or use our webmail program. You can also use encryption to protect your privacy.
VCN offers our users a web-based email (or Webmail), as an alternative to Outlook, Eudora, etc, called SquirrelMail. This means you can check your email from any computer with an Internet connection. To access your email account, you can log in at the top of our VCN home page, or you can go to the SquirrelMail home page at <a style="text-decoration: none;" href="http://mail.vcn.bc.ca/" target="_blank">mail.vcn.bc.ca.</a>
All registered users and community groups get webspace of up to 10 MB in size and can transfer files using File Transfer Protocol (FTP). Web site addresses are:
VCN’s Technical Help Desk is located at our main office at 280 – 111 West Hastings St. Vancouver, BC, V6B 1H4. The Help Desk staff is mostly made up of volunteers who are trained to deal with a wide range of tasks and problems and are available to speak to you in person, by phone: 778.724.0826 or by email: <a style="text-decoration: none;" href="mailto:help@vcn.bc.ca">help@vcn.bc.ca</a>
<a style="text-decoration: none;" title="Free Geek" href="http://freegeekvancouver.org/" target="_blank">Free Geek</a>, a non-profit organization in Vancouver, specializes in collecting computers, refurbishing them and giving them back to its volunteers through its Computer Adoption Program.
Non-profit groups that have a domain name (ex. www.groupname.org) hosted with Vancouver Community Network are entitled to 10 email accounts (ex. info@groupname.org, staff@groupname.org) for their organizational use.
<h4>WordPress</h4>
<h4>Free Mailing Lists</h4>
<h4>TechTeam</h4>
VCN provides groups with a sub-domain (eg. <b><i>vcn.bc.ca/yourgroup</i></b> or <b><i>yourgroup.vcn.bc.ca</i></b> for WebGUI installations) as part of its standard package of services. However, some groups prefer to register their own domain name (eg. <b><i>yourgroup.com</i></b>).
Presently, VCN hosts Domain Names for non-profit Community Groups for the .ca, .net, .org and .com top-level domains. VCN does not charge this service but we do request that groups that are able to do so, make a <a style="text-decoration: none;" href="/donate/" target="_blank">donation</a> as a Subscriber Supporter.
Non-profit groups that have a domain name (ex. www.groupname.org) hosted with Vancouver Community Network are entitled to 10 email accounts (ex. info@groupname.org, staff@groupname.org) for their organizational use.
<h4>WordPress</h4>
<h4>Free Mailing Lists</h4>
<h4>TechTeam</h4>
Please fill out the information below as accurately as possible. Account requests without a full legal name (on government-issued ID) and address will not be processed.
To view or change your VCN account information please send a request to VCN's privacy officer by using the following methods:
<h3>VCN outgoing mail service set up guide</h3>
<strong>VCN outgoing mail service for Android</strong>
<h3><strong>VCN outgoing mail service for iPhone
<h3>Thunderbird</h3>
<h3>Apple Mail</h3>
<h3>Outlook Express</h3>
<h3>WIndows Live Mail</h3>
<h3>Gmail</h3>
<ol>
&nbsp;
To setup auto-respond and Email Forwarding
<span style="color: #008b8b;"><strong>Fill out this form to submit your questions/issues to the Site Administrator.</strong></span>
<ol>
<p>
Here are the instructions for <strong>Outlook 2003</strong> in case you were interested to know.
1. Start by opening Microsoft outlook 2007. If you don't know how to start the program, do either of the following:
Here are the instructions for <strong>Outlook 2010</strong> in case you were interested to know.
<ol>
(<span style="color: #0000ff;"><span style="color: #008080;">A downloadable and printable version of this document, in Microsoft Word 97 format, is available <a href="http://www2.vcn.bc.ca/wp-content/uploads/2013/12/Using-the-Quiz-and-Survey-Master-plug-in-with-WordPress.doc">here</a>.</span>)</span>
(This document is based on Windows 7, however the steps also apply for Windows XP.)
[Form id="18"]]]></content:encoded>
VCN is thinking about providing technical support and public computer access at <a href="https://goo.gl/maps/u1nx3HDb95m" target="_blank" rel="noopener noreferrer">312 Main St</a> and we would like your feedback.
Please fill out the information below as accurately as possible. Account requests without a full legal name (on government-issued ID) and address will not be processed.
<table border="0" cellspacing="2" cellpadding="2">
Use the table below to update your client with the correct information.
<h1>Dealing with spam (comments)</h1>
