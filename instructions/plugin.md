# Plugin Notes

## All-in-One Event Calendar *(Keep)*

Replace the text on the calendar page with the following:

```html
<p>You need to login to post new events. The user name is "events" and the password is also "events".</p>

<p>In the title please include the name of the event and the date the event is scheduled for.</p>

<p>Log in <a herf="/wp-admin">here</a></p>

```
## Business Directory Plugin *(Keep)*

Could turn off "require login to post listing?" to allow user add new
address. Would still need approve before publishing


## Menu Image *(Keep)*

Used for a menu item in the top navigation

## Delightful Downloads *(Keep)*

Expands the use of with adding minutes sections

## Collapse-O-Matic *(Keep)*

Used in sign up page, and will be use for the new theme's widget

## WP Font Awesome *(Install)*

Font was download into the theme. It will now instead a plugin used in
various places in the website

## Slide Anything – Responsive Content / HTML Slider and Carousel *(Install)*

Replace the following plugins:

* WOW Slider
* New RoyalSlider
* Smart Slider 2

It will be use in the front page

## List category posts *(Install)*

This will add news post in a list, use by a widget and home page.

Also replaces the broken *Posts per Cat*. The new plugin appears to be
forked from the old plugin.

Pages that will use this plugin are:

* https://www2.vcn.bc.ca/troubleshooting/common-mailing-list-issues/
* https://www2.vcn.bc.ca/troubleshooting/common-web-hosting-issues/
* https://www2.vcn.bc.ca/troubleshooting/dialupissue/
* https://www2.vcn.bc.ca/troubleshooting/common-issues-with-donations/
* https://www2.vcn.bc.ca/troubleshooting/common-vcn-account-issues/

## Plugin being used

* Link Library
	* https://www2.vcn.bc.ca/community-resources/freenet/
* Form Maker? 
	* https://www2.vcn.bc.ca/services/services-for-groups/domain-name-hosting/
	* https://www2.vcn.bc.ca/free-group-email-accounts-pop3/
	* https://www2.vcn.bc.ca/free-cms-web-site-powered-by-wordpress/
	* https://www2.vcn.bc.ca/312-main-st/
	* https://www2.vcn.bc.ca/this-is-a-poll-test/
* Contact Form 7
	* https://www2.vcn.bc.ca/contact-us-2/  (Private)
* TablePress
	* https://www2.vcn.bc.ca/email-configuration/
* Simple Sitemap
	* https://www2.vcn.bc.ca/sitemap/ (Page is linked in the footer)
* Simple Pagination
	* https://www2.vcn.bc.ca/category/News

## Current Plugin List

|Plugin Name						 |Status |Comments                 |
|------------------------------------|-------|-------------------------|
|All-in-One Event Calendar by Time.ly|Keep   |Requested                |
|Autoptimize                         |Keep   |Optimization (count: 1)  |
|Business Directory Plugin           |Keep   |Requested                |
|Cache Enabler                       |Keep   |Optimization (count: 2)  |
|Captcha                             |Remove?|Not seen being used      |
|Collapse-O-Matic                    |Keep   |Used in widgets          |
|Contact Form 7                      |Remove?|Use in 1 private page    |
|Custom Taxonomies Menu Widget       |Remove |not using this widget    |
|Debug Bar                           |Keep   |Web Development/Security (count: 1)|
|Delightful Downloads                |Keep   |Requested                |
|Expand + Collapse Funk              |Remove?|Not seen being used      |
|Form Maker                          |Remove?|being use in various pages, but not all forms use this|
|Hide This                           |Remove?|seen being use in Events, but the page content is being replaced|
|Hierarchical Link Categories        |Remove |WordPress has it built in|
|iThemes Security                    |Keep   |Web Development/Security (count: 2)|
|Jetpack by WordPress.com            |Remove |Are either unused widget or needs a log in|
|Link Library                        |Remove |at least 1 Page is using it|
|Link Market's LDMS                  |Remove |Needs an active LDMS subsription|
|links category widget               |Remove |Not seen used & has a replacement|
|Menu Image                          |Keep   |Main menu home page use it|
|My Link Order                       |Keep?  |Not seen used, and My Link Order can replace it|
|New RoyalSlider                     |Remove |has a replacement        |
|P3 (Plugin Performance Profiler)    |Keep   |Optimization (count: 3)  |
|Peter's Login Redirect              |Remove |Use by event page to redirect user back to community event page|
|Post title marquee scroll           |Remove |Not using in the theme   |
|Posts per Cat [Unmaintained]        |Remove |Replaced by new plugin   |
|Query Monitor                       |Keep   |Web Development/Security (count: 3)|
|Restrict Categories                 |Remove?|Limits which category a post can have|
|Simple Pagination                   |Keep?  |Used to change the text in archive pages?|
|Simple Sitemap                      |Keep?  |Has a page in the footer that uses it|
|Single Category Permalink           |Keep?  |Will mess up the link for some posts if remove|
|Smart Slider 2                      |Remove |has a replacement        |
|Social Marketing                    |Remove |Activiation is incompleted|
|TablePress                          |Keep?  |at least 1 page is using it|
|Widget CSS Classes                  |Remove |New theme does not need this|
|WordPress Importer                  |Remove |This is an importer      |
|WOW Slider                          |Remove |has a replacement        |
|WP Dynamic Links                    |Remove |The 2 link are not being used, or even work properly|
|WP-Polls                            |Remove?|Not seen being used      |
