# Home Page - Bottom Carousel

Carousel setting:

![settings](carousel-settings.png)

Carousel style:

![style](carousel-style.png)

Item display: 

![display](carousel-display.png)

**Remember the short code, as it is needed for the home page.**

## Carousel 1

![Carousel 1](carousel-1.png)

```html
<h5>MEDIA</h5>
<p>Follow VCN on social media to find out what we have been up to. Also to communicate with us. We welcome your questions and input!</p>
<p>
<a href="http://www.facebook.com/vcn.community" target="_blank" class="fa fa-facebook-square fa-2x fa-fw"></a>
<a href="https://twitter.com/VCN_Community#" target="_blank" class="fa fa-twitter-square fa-2x fa-fw"></a>

<!-- uses css for images -->
<a href="http://internships.vcn.bc.ca/sample-page/" class="fa nav-capyi fa-2x fa-fw"></a>
</p>
```

## Carousel 2

![Carousel 2](carousel-2.png)

```html
<h5>DONATE</h5>
<p> VCN gratefully accepts donations of money and equipment, however large or small, and offers tax receipts for amounts of $25 or more.</p>
<p><a href="/get-involved/donate/" class="read-more" target="_blank">read more</a></p>
```

## Carousel 3

![Carousel 3](carousel-3.png)

```html
<h5>SERVICES</h5>
<p>Check out the services that VCN offers absolutely free of charge! To help you troubleshoot problems we also offer a help desk.</p>
<p><a href="/services/" target="_blank" class="read-more">read more</a></p>
```

## Carousel 4

![Carousel 4](carousel-4.png)

```html
<h5>WEBTEAM</h5>
<p>The webteam is passionate about designing quality and innovative communication materials for non-profits at a cost-effective price.</p> 
<p><a href="http://vcnlabs.com" target="_blank" class="read-more">read more</a></p>
```

## Carousel 5

![Carousel 5](carousel-5.png)

```html
<h5>TECHTEAM</h5>
<p>The TechTeam is comprised of a highly trained team who use technology to provide non-profit organizations with fast, cost-effective IT services.</p> 
<p><a href="/techteam/" class="read-more" target="_blank">read more</a></p>
```

## Carousel 6

![Carousel 6](carousel-6.png)

```html
<h5>INTERNSHIPS</h5>
<p>The Youth Internships (YI) program gives students and young professionals an opportunity to gain experience by working in non-profits.</p> 
<p><a href="http://internships.vcn.bc.ca/" class="read-more" target="_blank">read more</a></p>
```

## Carousel 7

This slide is useless.

![Carousel 7](carousel-7.png)

```html
<h5>BLOG</h5>
<p> On the VCN Blog, you can read Technology news and updates from the world of non-profit organizations. Visit us and comment!</p> 
<p><a class="read-more">coming soon </a></p>
```
