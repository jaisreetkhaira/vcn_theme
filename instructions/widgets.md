# Widgets:
## Header Right

### Header Right - Top widget

Text for the widget:

![header right top wiget](header-right-top.png)

```html
<div class="media-fill-nav">
	
	<span id="mail" class="collapseomatic" rel="mail">Mail Login</span>
	[themeimg src="orange-line.png" alt="slash"]
	<i id="search" class="fas fa-search collapseomatic" aria-hidden="true" rel="search"></i>
	<div id="target-search" class="collapseomatic_content" >[wpbsearch]</div>
	<div id="target-mail" class="collapseomatic_content">
		<form method="post" 
			  action="https://mail.vcn.bc.ca/src/redirect.php" 
			  name="f">
		<label> 
			Login: <input name="login_username" type="text" size="8">
			@vcn.bc.ca
		</label>
		<label>
			Password: <input name="secretkey" type="password" size="8">
		</label>
		<input type="submit" value="Login">
		<br/>
		<a href="/sign-up/">Sign Up!</a>
		</form>
	</div>
</div>
```

### Header Right - bottom widget

![header right bottom wiget](header-right-bottom.png)

Text for the widget:

```html
<!-- uses Font Awesome for images -->
<a href="http://www.facebook.com/vcn.community" target="_blank" class="fab fa-facebook-square fa-2x fa-fw"></a>
<a href="https://twitter.com/VCN_Community#" target="_blank" class="fab fa-twitter-square fa-2x fa-fw"></a>

<!-- uses css for images -->
<a href="http://internships.vcn.bc.ca/sample-page/" class="fas nav-capyi fa-2x fa-fw"></a>
<a href="https://www2.vcn.bc.ca/donate/" class="fas nav-donate fa-2x fa-fw"></a>
```

## Side Bars

### Side Bar - Top

![side bar top wiget](side-bar-top.png)

### Side Bar - Bottom

![side bar bottom wiget](side-bar-bottom.png)

Text for the widget:

```html
[catlist name=news]
<div>
	<a href="/category/news/">More news...</a>
</div>
```

## Footer 1

This widget already exists in the old theme.

![footer #1 widget](footer-widget-1.png)

Contents of the widget:

![footer #1 widget](footer-1.png)

## Footer 2

This widget is already exists in the old theme.

![footer #2 widget](footer-widget-2.png)

Contents of the widget:

![footer #2 widget](footer-2.png)

## Footer 3

This widget is already exists in the old theme.

![footer #3 widget](footer-widget-3.png)

Contents of the widget:

![footer #3 widget](footer-3.png)

## Footer 4 

This widget is already exists in the old theme, but with some edits.

![footer #4 widget](footer-widget-4.png)

```html
Contents of the widget:
<p>
	help|desk:<br />
Tel: 778.724.0826
</p>
<p>
tech|team:<br />
Tel: 778.724.0626
</p>
<div>
	<a href="/user-agreement/">User Agreement</a>
</div>
<div>
<a href="/policy/">Privacy Policy</a>
</div>
```
