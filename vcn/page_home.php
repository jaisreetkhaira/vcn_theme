<?php
/*
Template Name: Home
*/
//* Add landing body class to the head
add_filter( 'body_class', 'minimum_add_body_class' );
function minimum_add_body_class( $classes ) {
	$classes[] = 'minimum-landing';
	return $classes;
}

//* Unregister content/sidebar layout setting
genesis_unregister_layout( 'content-sidebar' );

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Run the Genesis loop
genesis();
